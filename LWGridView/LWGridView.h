//
//  LWGridView.h
//
//  Created by scott.8an@gmail.com on 4/25/12.
//

#import <UIKit/UIKit.h>

@class LWGridView;

/*******************************
 * protocol
 ******************************/
@protocol LWGridViewDelegate <NSObject>
//delegate
- (void)gridView:(LWGridView*)gridView didGridClickedAtIndex:(NSInteger)index;
- (void)didSeeMoreButtonClickedOfGridView:(LWGridView*)gridView;

//data source
- (UIView*)gridView:(LWGridView*)gridView viewForGridAtIndex:(NSInteger)index;
- (NSString*)gridView:(LWGridView*)gridView titleForGridAtIndex:(NSInteger)index;
@end

@interface LWGridView : UITableViewCell {
    id<LWGridViewDelegate>delegate;
    
    BOOL     isExpansion;
    NSMutableArray *contextArray;
}
@property (assign) id<LWGridViewDelegate>       delegate;
@property (assign) BOOL                         isExpansion;
@property (nonatomic,copy)    NSMutableArray    *gridArray;
@property (nonatomic,retain)    NSString        *title;
@property (nonatomic,retain)    UIImage         *backgroundImage;

+ (CGFloat)heightForLWGridViewWithDataArray:(NSArray*)arr;
@end
