//
//  LWGridView.h
//
//  Created by scott.8an@gmail.com on 4/25/12.
//


#import "LWGridView.h"

@implementation LWGridView
@synthesize gridArray,title,delegate,backgroundImage,isExpansion;

- (void)dealloc{
    [contextArray release];
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.clipsToBounds = YES;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        contextArray = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

- (void)initUIWithRect:(CGRect)rect atIndex:(int)index{
    if (delegate && [delegate respondsToSelector:@selector(gridView:viewForGridAtIndex:)]) {
        UIView *view = [delegate gridView:self viewForGridAtIndex:index];
        view.userInteractionEnabled = YES;
        
        if (view) {
            view.frame = rect;
            [self addSubview:view];
            
            if (delegate && [delegate respondsToSelector:@selector(gridView:titleForGridAtIndex:)]) {
                NSString *gridTile = [delegate gridView:self titleForGridAtIndex:index];
                
                if (gridTile && [gridTile length]) {
                    //覆盖
                    UIButton *cover = [UIButton buttonWithType:UIButtonTypeCustom];
                    cover.backgroundColor = [UIColor blackColor];
                    cover.alpha = 0.5f;
                    cover.frame = CGRectMake(rect.origin.x, rect.origin.y+rect.size.height*2/3+15, rect.size.width, rect.size.height/3-15);
                    [self addSubview:cover];
                    
                    UILabel *gridTileLabel = [[UILabel alloc] init];
                    gridTileLabel.frame = CGRectMake(rect.origin.x, rect.origin.y+rect.size.height*2/3+15, rect.size.width, rect.size.height/3-15);
                    gridTileLabel.font = [UIFont boldSystemFontOfSize:11];
                    gridTileLabel.textColor = [UIColor whiteColor];
                    gridTileLabel.backgroundColor = [UIColor clearColor];
                    gridTileLabel.textAlignment = UITextAlignmentCenter;
                    gridTileLabel.text = gridTile;
                    [self addSubview:gridTileLabel];
                    [gridTileLabel release];
                }
            }
            
            
            //事件出发
            UIButton *actionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            actionBtn.frame = rect;
            actionBtn.backgroundColor = [UIColor clearColor];
            actionBtn.tag = index;
            [actionBtn addTarget:self action:@selector(clickTag:) forControlEvents:UIControlEventTouchDown];
            [self addSubview:actionBtn];
        }
    }
}

- (void)clickTag:(UIButton*)sender{
    if (delegate && [delegate respondsToSelector:@selector(gridView:didGridClickedAtIndex:)]) {
        [delegate gridView:self didGridClickedAtIndex:sender.tag];
        //        NSLog(@"-->current index is :%i",sender.tag);
    }
}

- (void)clickToShowMore{
    if (delegate && [delegate respondsToSelector:@selector(didSeeMoreButtonClickedOfGridView:)]) {
        self.isExpansion = NO;
        [delegate didSeeMoreButtonClickedOfGridView:self];
    }
}

- (void)drawRect:(CGRect)rect{
    if (gridArray && [gridArray count]) {
        //配置格子数目
        //格子数目
        NSInteger gridsNum = [gridArray count];
        if (gridsNum<1) {
            return;
        }
        [contextArray removeAllObjects];
        [contextArray addObjectsFromArray:gridArray];
        
        
        if (isExpansion && [contextArray count]>5) {
            [contextArray removeObjectsInRange:NSMakeRange(5, [contextArray count]-5)];
            
            //底部背景图
            UIView *bgColorView = [[UIView alloc] initWithFrame:CGRectMake(320-100-13.5/2-3, 94+15, 94, 94)];
            bgColorView.backgroundColor = [UIColor colorWithRed:80/255.0 green:136/255.0 blue:214/255.0 alpha:1];
            [self addSubview:bgColorView];
            
            
            UILabel *extraTagsCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, bgColorView.frame.size.width, bgColorView.frame.size.height)];
            extraTagsCountLabel.text = [NSString stringWithFormat:@"+%i",gridsNum-5];
            extraTagsCountLabel.textColor = [UIColor whiteColor];
            extraTagsCountLabel.textAlignment = UITextAlignmentCenter;
            [extraTagsCountLabel setFont:[UIFont boldSystemFontOfSize:30]];
            extraTagsCountLabel.backgroundColor = [UIColor clearColor];
            [bgColorView addSubview:extraTagsCountLabel];
            [extraTagsCountLabel release];
            
            //覆盖
            UIButton *cover = [UIButton buttonWithType:UIButtonTypeCustom];
            cover.backgroundColor = [UIColor blackColor];
            cover.alpha = 0.5f;
            cover.frame = CGRectMake(0, bgColorView.frame.size.height-16, bgColorView.frame.size.width, 16);
            [bgColorView addSubview:cover];
            
            //点击查看更多的标签文字
            UILabel *showMoreTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, bgColorView.frame.size.height-16, bgColorView.frame.size.width, 16)];
            showMoreTitle.backgroundColor = [UIColor blackColor];
            showMoreTitle.text = @"点击查看更多";
            showMoreTitle.textColor = [UIColor whiteColor];
            showMoreTitle.textAlignment = UITextAlignmentCenter;
            [showMoreTitle setFont:[UIFont boldSystemFontOfSize:11]];
            showMoreTitle.backgroundColor = [UIColor clearColor];
            [bgColorView addSubview:showMoreTitle];
            [showMoreTitle release];
            
            //顶部事件按钮
            UIButton *addMoreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            addMoreBtn.frame = CGRectMake(320-100-13.5/2-3, 94+15, 94, 94);
            [addMoreBtn addTarget:self action:@selector(clickToShowMore) forControlEvents:UIControlEventTouchUpInside];
            addMoreBtn.backgroundColor = [UIColor clearColor];
            [self addSubview:addMoreBtn];
        }
        
        int needToShowTagCount = [contextArray count];//gridsNum
        int lineNumber = needToShowTagCount/3;
        int extraTagNumber = needToShowTagCount%3;
        int verSpace = 3;//纵向标签之间距离
        int horSpace = 3;//横向标签之间距离
        
        int myBaseY = 13.5/2+verSpace;
        
        //用来保存当前
        CGPoint currentPoint = CGPointZero;
        CGSize  tagPicSize = CGSizeMake(94, 94);
        
        CGFloat leftMargin  = 13.5;
        
        int telCurrentTagNumber = 0;//指向当前的图片按钮
        
        
        //画背景图
        if (backgroundImage) {
            UIImage *newImg = [backgroundImage stretchableImageWithLeftCapWidth:0 topCapHeight:backgroundImage.size.height/2];
            int extra = extraTagNumber>0?1:0;
            int height = 13.5/2+verSpace+lineNumber*94+verSpace*lineNumber+extra*94+verSpace+13.5/2+verSpace;
            [newImg drawInRect:CGRectMake(rect.origin.x, rect.origin.y, rect.size.width,height+18)];
        }
        
        if (needToShowTagCount<4) {
            for (int i=0; i<needToShowTagCount; i++) {
                int baseX =leftMargin+3+horSpace*i+tagPicSize.width*i;
                CGRect myRect = CGRectMake(baseX, myBaseY, tagPicSize.width, tagPicSize.height);
                
                [self initUIWithRect:myRect atIndex:telCurrentTagNumber];
                telCurrentTagNumber++;
            }
        }else{
            // NSLog(@"===总数：%i,需画行数:%i,余数:%i",needToShowTagCount,lineNumber,extraTagNumber);
            for (int i=0; i<lineNumber; i++) {
                int baseX = leftMargin+3;
                int baseY = myBaseY+verSpace+verSpace*i+tagPicSize.height*i;
                for (int j=0; j<3; j++) {
                    baseX = leftMargin+3+horSpace*j+tagPicSize.width*j;
                    
                    currentPoint = CGPointMake(baseX, baseY);
                    
                    CGRect myRect = CGRectMake(currentPoint.x, currentPoint.y, tagPicSize.width, tagPicSize.height);
                    
                    [self initUIWithRect:myRect atIndex:telCurrentTagNumber];
                    
                    telCurrentTagNumber++;
                }
            }
            
            //重设当前坐标的横坐标
            currentPoint.x = leftMargin+3;
            //增加当前坐标纵坐标到下一个标签位置
            currentPoint.y+=tagPicSize.height+verSpace;
            // NSLog(@"===========当前的坐标====%f,%f",currentPoint.x,currentPoint.y);
            
            if (extraTagNumber) {
                for (int i=0; i<extraTagNumber; i++) {
                    int baseX = leftMargin+3+horSpace*i+tagPicSize.width*i;
                    int baseY = currentPoint.y;
                    
                    CGRect myRect = CGRectMake(baseX, baseY, tagPicSize.width, tagPicSize.height);
                    
                    [self initUIWithRect:myRect atIndex:telCurrentTagNumber];
                    telCurrentTagNumber++;
                }
            }
        }
    }
}

- (void)setIsExpansion:(BOOL)expansion{
    isExpansion = expansion;
    [self setNeedsDisplay];
}

+ (CGFloat)heightForLWGridViewWithDataArray:(NSArray*)arr{
    int needToShowTagCount = [arr count];
    int lineNumber = needToShowTagCount/3;
    int extraTagNumber = needToShowTagCount%3>0?1:0;
    
    
    int extra = extraTagNumber>0?1:0;
    int height = 13.5/2+3+lineNumber*94+3*lineNumber+extra*94+3+13.5/2+3+18;
    
    return height;
}
@end
