//
//  LWGridView.h
//
//  Created by scott.8an@gmail.com on 4/25/12.
//  Copyright (c) 2012 roosher. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
